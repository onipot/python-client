from .AnnotationsResponse import AnnotationsResponse
from .AudioResponse import AudioResponse
from .ClassificationResponse import ClassesResponse, ClassificationResponse
from .TextsResponse import TextsResponse, TextsResponseObject
from .utils import get_response
